package com.websystique.springmvc.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "orderItemPrinteds")

public class OrderItemPrinted {

    @Id
    private long id;

    private long orderId;
	
    private Long parentId;

    private String name;

    private long menuItemId=1l;
    private boolean isSubMenuItem=false;
    
    
private String price;


private String type;

private Long orderItemId;



	public OrderItemPrinted() {}



	
    public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}


	
	



	public long getOrderId() {
		return orderId;
	}




	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}




	public Long getParentId() {
		return parentId;
	}




	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getPrice() {
		return price;
	}




	public void setPrice(String price) {
		this.price = price;
	}




	public String getType() {
		return type;
	}




	public void setType(String type) {
		this.type = type;
	}




	public Long getOrderItemId() {
		return orderItemId;
	}




	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}




	@Override
    public String toString() {
        return String.format(
                "OrderItemPrinted[id=%s,orderId='%s',parentId='%s',name='%s',price='%s',type='%s',orderItemId='%s']",
                id, orderId,parentId, name, price,  type, orderItemId);
    }

}

