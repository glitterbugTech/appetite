package com.websystique.springmvc.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "users")
public class User {

	 @Id
	private long id;
	
	private String username;
	
	private String address;
	
	private String email;
	
	public User(){

	}
	
	public User(String username, String address, String email){
		this.username = username;
		this.address = address;
		this.email = email;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


    @Override
    public String toString() {
        return String.format(
        		 "User[id=%s, username='%s', address='%s', email='%s']",
        	        id, username, address, email);
    }

	public long getId() {
	
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	


}
