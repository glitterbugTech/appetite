package com.websystique.springmvc.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sequence")
public class SequenceId {

	@Id
	private String id;

	private long seq;

	public SequenceId()
	{
		
	}
	
	public SequenceId(String id)
	{
		this.id=id;
		this.seq =1L;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}
	
    @Override
    public String toString() {
        return String.format(
                "SequenceId[id=%s, seq='%s']",
                id, seq);
    }

	
	
}