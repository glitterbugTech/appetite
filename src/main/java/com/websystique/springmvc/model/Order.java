package com.websystique.springmvc.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "orders")

public class Order {

    @Id
    private long id;


    private long customerId;
	
    private Long lastUpdated;

    private String requiredASAP;

    
private String requiresDelivery;


private String paymentMethod;

private String expectedDeliveryTime;


    public String order_status;
    public String dateOrdered="";
	
	
	public Order() {}


	
	
	
    public long getId() {
		return id;
	}





	public void setId(long id) {
		this.id = id;
	}





	public long getCustomerId() {
		return customerId;
	}





	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}





	public Long getLastUpdated() {
		return lastUpdated;
	}





	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}





	public String getRequiredASAP() {
		return requiredASAP;
	}





	public void setRequiredASAP(String requiredASAP) {
		this.requiredASAP = requiredASAP;
	}





	public String getRequiresDelivery() {
		return requiresDelivery;
	}





	public void setRequiresDelivery(String requiresDelivery) {
		this.requiresDelivery = requiresDelivery;
	}





	public String getPaymentMethod() {
		return paymentMethod;
	}





	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}





	public String getExpectedDeliveryTime() {
		return expectedDeliveryTime;
	}





	public void setExpectedDeliveryTime(String expectedDeliveryTime) {
		this.expectedDeliveryTime = expectedDeliveryTime;
	}








	public String getOrder_status() {
		return order_status;
	}





	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}





	public String getDateOrdered() {
		return dateOrdered;
	}





	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}





	@Override
    public String toString() {
        return String.format(
                "Order[id=%s,customerId='%s',lastUpdated='%s',requiredASAP='%s',requiresDelivery='%s',paymentMethod='%s',expectedDeliveryTime='%s',order_status='%s',dateOrdered='%s']",
                id, customerId, lastUpdated, requiredASAP, requiresDelivery,  paymentMethod, expectedDeliveryTime, order_status, dateOrdered);
    }

}

