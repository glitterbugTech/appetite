package com.websystique.springmvc.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.mongodb.MongoClient; 
import com.mongodb.MongoCredential; 
import com.mongodb.ServerAddress; 
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import java.util.Arrays;


@Configuration
@EnableWebMvc

@ComponentScan({"com.websystique.springmvc"})
public class HelloWorldConfiguration extends WebMvcConfigurerAdapter{
	
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		registry.viewResolver(viewResolver);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
	}
	
	
	@Bean
	    public MongoDbFactory mongoDbFactory() throws Exception {
		

 MongoCredential credential = MongoCredential.createCredential("glitterbug11", "glitterbug11", "Clamper11".toCharArray());
    ServerAddress serverAddress = new ServerAddress("172.30.72.195", 27017);

	
	  MongoClient mongoClient = new MongoClient(serverAddress,Arrays.asList(credential)); 

    // Mongo DB Factory
    SimpleMongoDbFactory simpleMongoDbFactory = new SimpleMongoDbFactory(
            mongoClient, "glitterbug11");

		        return simpleMongoDbFactory;
	    }
		
	 
	    @Bean
	    public MongoTemplate mongoTemplate() throws Exception {
	        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
	        return mongoTemplate;
	    }
	 
	    /*
		
		ipaddress = 	172.30.72.195
		
       Username: glitterbug11
       Password: Clamper11
  Database Name: glitterbug11
 Connection URL: mongodb://glitterbug11:Clamper11@mongodb/glitterbug11
  Connection URL: mongodb://glitterbug11:Clamper11@mongodb/glitterbug11
 */
	    

}