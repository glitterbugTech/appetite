package com.websystique.springmvc.service;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.websystique.springmvc.model.Order;
import com.websystique.springmvc.repo.SequenceDao;
import com.websystique.springmvc.repo.OrderRepository;





@Service("orderService")
public class OrderServiceImpl implements OrderService{
	
	@Autowired
	private OrderRepository orderRespository;
		
	private static List<Order> orders;
	
	private static final String USER_SEQ_KEY = "order_0";

	@Autowired
	private SequenceDao sequenceDao;
	

	public List<Order> findAll() {
		
		
		orders = orderRespository.findAll();
		if(orders ==null || orders.isEmpty())
		{
			populateDummyOrders();
			orders = orderRespository.findAll();
		}

		return orders;
	}
	
	public Order findById(long id) {
		orders = orderRespository.findAll();
		for(Order order : orders){
			if(order.getId() == id){
				return order;
			}
		}
		return null;
	}
	
	public List<Order> findByDate(Long lastUpdated) {
		
		return orderRespository.findByLastUpdated(lastUpdated);
		
		
	}
	
	public void save(Order order) {
		order.setId(sequenceDao.getNextSequenceId(USER_SEQ_KEY));
		
		orderRespository.create(order);
	//	repository.save(new Order("Alice", "","Smith"));

		//order.setId(counter.incrementAndGet());
		//orders.add(order);
	}

	public void update(Order order) {
		
			
		if(order!=null)
		{

			orderRespository.update(order);
		}



	}



	public void deleteOrderById(Long id) {
		
		Order	orderNew = orderRespository.findById(id);
		if(orderNew!=null)
		{
			orderRespository.delete(orderNew.getId());
			
		}
	}
	
	
	private Object findById(Long id) {
		
		Order c = orderRespository.findById(id);
		if( c == null)
		{
			return null;
		}
		return c;
	}

	public void deleteAll(){
		orderRespository.deleteAll();
	}

	private  void populateDummyOrders(){
		List<Order> orders = new ArrayList<Order>();
		Order test1= new Order();
		test1.setPaymentMethod("order1");
		test1.setCustomerId(1l);
		test1.setDateOrdered("1-1-2017");
		test1.setExpectedDeliveryTime("2.00pm");
		test1.setLastUpdated(100l);
		test1.setRequiredASAP("true");
		test1.setRequiresDelivery("true");
		test1.setOrder_status("new");

		
	     save(test1);

	}

	@Override
	public void delete(long id) {
		orderRespository.delete(id);
		
	}

	@Override
	public boolean isOrderExist(Order order) {

		Order c = (Order) findById(order.getId());
		if(c==null)
		{
			return false;
		}
		
		return true;
	}

	@Override
	public List<Order> findByCustomerId(Long customerId) {
		// TODO Auto-generated method stub
		return orderRespository.findByCustomerId(customerId);
	}





}
