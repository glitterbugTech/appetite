package com.websystique.springmvc.service;

import java.util.List;

import com.websystique.springmvc.model.OrderItemPrinted;




public interface OrderItemPrintedService {
	
	OrderItemPrinted findById(long id);
	
    public  List < OrderItemPrinted > findByOrderItemId(Long orderItemId);
    public  List < OrderItemPrinted > findByOrderId(Long orderId);
	
	void save(OrderItemPrinted orderItemPrinted);
	
	void update(OrderItemPrinted orderItemPrinted);
	
	void delete(long id);

	List<OrderItemPrinted> findAll(); 
	
	void deleteAll();
	
	public boolean isOrderItemPrintedExist(OrderItemPrinted orderItemPrinted);
	
}
