package com.websystique.springmvc.service;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.websystique.springmvc.model.OrderItemPrinted;
import com.websystique.springmvc.repo.SequenceDao;
import com.websystique.springmvc.repo.OrderItemPrintedRepository;





@Service("orderItemPrintedService")
public class OrderItemPrintedServiceImpl implements OrderItemPrintedService{
	
	@Autowired
	private OrderItemPrintedRepository orderItemPrintedRespository;
		
	private static List<OrderItemPrinted> orderItemPrinteds;
	
	private static final String USER_SEQ_KEY = "orderItemPrinted_0";

	@Autowired
	private SequenceDao sequenceDao;
	

	public List<OrderItemPrinted> findAll() {
		
		
		orderItemPrinteds = orderItemPrintedRespository.findAll();
		if(orderItemPrinteds ==null || orderItemPrinteds.isEmpty())
		{
			populateDummyOrderItemPrinteds();
			orderItemPrinteds = orderItemPrintedRespository.findAll();
		}

		return orderItemPrinteds;
	}
	
	public OrderItemPrinted findById(long id) {
		orderItemPrinteds = orderItemPrintedRespository.findAll();
		for(OrderItemPrinted orderItemPrinted : orderItemPrinteds){
			if(orderItemPrinted.getId() == id){
				return orderItemPrinted;
			}
		}
		return null;
	}
	
	public List<OrderItemPrinted> findByOrderItemId(Long orderItemId) {
		
		return orderItemPrintedRespository.findByOrderItemId(orderItemId);
		
		
	}
	
	public void save(OrderItemPrinted orderItemPrinted) {
		orderItemPrinted.setId(sequenceDao.getNextSequenceId(USER_SEQ_KEY));
		
		orderItemPrintedRespository.create(orderItemPrinted);
	//	repository.save(new OrderItemPrinted("Alice", "","Smith"));

		//orderItemPrinted.setId(counter.incrementAndGet());
		//orderItemPrinteds.add(orderItemPrinted);
	}

	public void update(OrderItemPrinted orderItemPrinted) {
		
			
		if(orderItemPrinted!=null)
		{

			orderItemPrintedRespository.update(orderItemPrinted);
		}



	}



	public void deleteOrderItemPrintedById(Long id) {
		
		OrderItemPrinted	orderItemPrintedNew = orderItemPrintedRespository.findById(id);
		if(orderItemPrintedNew!=null)
		{
			orderItemPrintedRespository.delete(orderItemPrintedNew.getId());
			
		}
	}
	
	
	private Object findById(Long id) {
		
		OrderItemPrinted c = orderItemPrintedRespository.findById(id);
		if( c == null)
		{
			return null;
		}
		return c;
	}

	public void deleteAll(){
		orderItemPrintedRespository.deleteAll();
	}

	private  void populateDummyOrderItemPrinteds(){
		OrderItemPrinted test1= new OrderItemPrinted();
		test1.setName("Order item name");
		test1.setOrderId(1l);
		test1.setPrice("11.00");
		test1.setType("parent");
		test1.setOrderItemId(2l);
		test1.setParentId(1l);
	
	     save(test1);

	}

	@Override
	public void delete(long id) {
		orderItemPrintedRespository.delete(id);
		
	}

	@Override
	public boolean isOrderItemPrintedExist(OrderItemPrinted orderItemPrinted) {

		OrderItemPrinted c = (OrderItemPrinted) findById(orderItemPrinted.getId());
		if(c==null)
		{
			return false;
		}
		
		return true;
	}

	@Override
	public List<OrderItemPrinted> findByOrderId(Long orderId) {
		
		return orderItemPrintedRespository.findByOrderId(orderId);
		
		
	}
	


}
