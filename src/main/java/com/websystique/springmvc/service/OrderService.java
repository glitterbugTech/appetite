package com.websystique.springmvc.service;

import java.util.List;

import com.websystique.springmvc.model.Order;




public interface OrderService {
	
	Order findById(long id);
	
	List < Order > findByDate(Long lastUpdated);
	
    List < Order > findByCustomerId(Long customerId);

	
	void save(Order order);
	
	void update(Order order);
	
	void delete(long id);

	List<Order> findAll(); 
	
	void deleteAll();
	
	public boolean isOrderExist(Order order);
	
}
