package com.websystique.springmvc.repo;

import java.util.List;


import com.websystique.springmvc.model.Order;


public interface OrderRepository  {

    public  List<Order> findByName(String name);       
    public  List < Order > findByLastUpdated(Long lastUpdated);
    public  List < Order > findByCustomerId(Long customerId);
    public List < Order > findAll();
    public void create(Order order);
    public void update(Order order);
    public void delete(Long id);
	public void deleteAll();
	Order findById(long id);
    
}
