package com.websystique.springmvc.repo;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.websystique.springmvc.model.Customer;

@Repository
@Qualifier("customerRepository")
public class CustomerRepositoryImpl implements CustomerRepository {

	@Autowired
    MongoTemplate mongoTemplate;
	
	   final String COLLECTION = "customer_0";
		 
	   public void create(Customer car) {
	        mongoTemplate.save(car,COLLECTION);
	    }
	 
	    public void update(Customer car) {
	        mongoTemplate.save(car, COLLECTION);
	    }
	 
	    public void delete(Long id) {
	    	  Query query = new Query(Criteria.where("_id").is(id));
	    	  Customer c =  mongoTemplate.findOne(query, Customer.class, COLLECTION);
	        mongoTemplate.remove(c, COLLECTION);
	    }
	    
	    
	 
	    public void deleteAll() {
	        mongoTemplate.remove(new Query(), COLLECTION);
	    }
	 
	    public Customer find(Customer customer) {
	        Query query = new Query(Criteria.where("_id").is(customer.getId()));
	        return mongoTemplate.findOne(query, Customer.class, COLLECTION);
	    }
	 
	    public List < Customer > findAll() {
	        return (List < Customer > ) mongoTemplate.findAll(Customer.class, COLLECTION);
	    }

		@Override
		public  List < Customer > findByName(String name) {
			 Query query = new Query(Criteria.where("name").is(name));
		        return (List<Customer>) mongoTemplate.find(query, Customer.class, COLLECTION);
		}

		@Override
		public Customer findByEmailAddress(String emailAddress) {
			 Query query = new Query(Criteria.where("emailAddress").is(emailAddress));
		        return mongoTemplate.findOne(query, Customer.class, COLLECTION);
		}
		
		@Override
		public Customer findByPhone(String phone) {
			 Query query = new Query(Criteria.where("phone").is(phone));
			 Customer returnedc = 
		         mongoTemplate.findOne(query, Customer.class, COLLECTION);
			 
			 return returnedc;
		}

		@Override
		public Customer findById(long id) {
			 Query query = new Query(Criteria.where("id").is(id));
		        return mongoTemplate.findOne(query, Customer.class, COLLECTION);
		}
		

}
