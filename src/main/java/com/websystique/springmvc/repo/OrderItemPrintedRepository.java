package com.websystique.springmvc.repo;

import java.util.List;


import com.websystique.springmvc.model.OrderItemPrinted;


public interface OrderItemPrintedRepository  {
    
    public  List < OrderItemPrinted > findByOrderItemId(Long orderItemId);
    public  List < OrderItemPrinted > findByOrderId(Long orderId);
    public List < OrderItemPrinted > findAll();
    public void create(OrderItemPrinted orderItemPrinted);
    public void update(OrderItemPrinted orderItemPrinted);
    public void delete(Long id);
	public void deleteAll();
	OrderItemPrinted findById(long id);
    
}
