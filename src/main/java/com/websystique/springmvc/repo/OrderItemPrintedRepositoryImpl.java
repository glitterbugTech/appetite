package com.websystique.springmvc.repo;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.websystique.springmvc.model.OrderItemPrinted;

@Repository
@Qualifier("orderItemPrintedRepository")
public class OrderItemPrintedRepositoryImpl implements OrderItemPrintedRepository {

	@Autowired
    MongoTemplate mongoTemplate;
	
	   final String COLLECTION = "orderItemPrinted_0";
		 
	   public void create(OrderItemPrinted orderItemPrinted) {
	        mongoTemplate.save(orderItemPrinted,COLLECTION);
	    }
	 
	    public void update(OrderItemPrinted orderItemPrinted) {
	        mongoTemplate.save(orderItemPrinted, COLLECTION);
	    }
	 
	    public void delete(Long id) {
	    	  Query query = new Query(Criteria.where("_id").is(id));
	    	  OrderItemPrinted c =  mongoTemplate.findOne(query, OrderItemPrinted.class, COLLECTION);
	        mongoTemplate.remove(c, COLLECTION);
	    }
	    
	    
	 
	    public void deleteAll() {
	        mongoTemplate.remove(new Query(), COLLECTION);
	    }
	 
	    public OrderItemPrinted find(OrderItemPrinted orderItemPrinted) {
	        Query query = new Query(Criteria.where("_id").is(orderItemPrinted.getId()));
	        return mongoTemplate.findOne(query, OrderItemPrinted.class, COLLECTION);
	    }
	 
	    public List < OrderItemPrinted > findAll() {
	        return (List < OrderItemPrinted > ) mongoTemplate.findAll(OrderItemPrinted.class, COLLECTION);
	    }


		@Override
		public OrderItemPrinted findById(long id) {
			 Query query = new Query(Criteria.where("id").is(id));
		        return mongoTemplate.findOne(query, OrderItemPrinted.class, COLLECTION);
		}


		@Override
		public List<OrderItemPrinted> findByOrderId(Long orderId) {
			 Query query = new Query(Criteria.where("orderId").is(orderId));
		        return mongoTemplate.find(query, OrderItemPrinted.class, COLLECTION);
		}
		
		@Override
		public List<OrderItemPrinted> findByOrderItemId(Long orderItemId) {
			 Query query = new Query(Criteria.where("orderItemId").is(orderItemId));
		        return mongoTemplate.find(query, OrderItemPrinted.class, COLLECTION);
		}
		
		
		
		

}
