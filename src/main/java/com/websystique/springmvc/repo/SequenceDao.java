package com.websystique.springmvc.repo;

import com.websystique.springmvc.exception.SequenceException;

public interface SequenceDao {

	long getNextSequenceId(String key) throws SequenceException;

}