package com.websystique.springmvc.repo;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.websystique.springmvc.model.Order;

@Repository
@Qualifier("orderRepository")
public class OrderRepositoryImpl implements OrderRepository {

	@Autowired
    MongoTemplate mongoTemplate;
	
	   final String COLLECTION = "order_0";
		 
	   public void create(Order order) {
	        mongoTemplate.save(order,COLLECTION);
	    }
	 
	    public void update(Order order) {
	        mongoTemplate.save(order, COLLECTION);
	    }
	 
	    public void delete(Long id) {
	    	  Query query = new Query(Criteria.where("_id").is(id));
	    	  Order c =  mongoTemplate.findOne(query, Order.class, COLLECTION);
	        mongoTemplate.remove(c, COLLECTION);
	    }
	    
	    
	 
	    public void deleteAll() {
	        mongoTemplate.remove(new Query(), COLLECTION);
	    }
	 
	    public Order find(Order order) {
	        Query query = new Query(Criteria.where("_id").is(order.getId()));
	        return mongoTemplate.findOne(query, Order.class, COLLECTION);
	    }
	 
	    public List < Order > findAll() {
	        return (List < Order > ) mongoTemplate.findAll(Order.class, COLLECTION);
	    }

		@Override
		public  List < Order > findByName(String name) {
			 Query query = new Query(Criteria.where("name").is(name));
		        return (List<Order>) mongoTemplate.find(query, Order.class, COLLECTION);
		}

	

		@Override
		public Order findById(long id) {
			 Query query = new Query(Criteria.where("id").is(id));
		        return mongoTemplate.findOne(query, Order.class, COLLECTION);
		}

		@Override
		public List<Order> findByLastUpdated(Long lastUpdated) {
			 Query query = new Query(Criteria.where("lastUpdated").gt(lastUpdated));
		        return mongoTemplate.find(query, Order.class, COLLECTION);
	
		}

		@Override
		public List<Order> findByCustomerId(Long customerId) {
			 Query query = new Query(Criteria.where("customerId").is(customerId));
		        return mongoTemplate.find(query, Order.class, COLLECTION);
		}
		
		
		

}
