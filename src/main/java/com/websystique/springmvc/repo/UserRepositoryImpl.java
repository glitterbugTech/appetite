package com.websystique.springmvc.repo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.websystique.springmvc.model.User;

@Repository
@Qualifier("userRespository")
public class UserRepositoryImpl implements UserRepository{
	
	@Autowired
    MongoTemplate mongoTemplate;
	
	   final String COLLECTION = "user_0";
	   
	    public void create(User car) {
	        mongoTemplate.insert(car, COLLECTION);
	    }
	 
	    public void update(User car) {
	        mongoTemplate.save(car, COLLECTION);
	    }
	 
	    public void delete(User car) {
	        mongoTemplate.remove(car, COLLECTION);
	    }
	    
	    
	 
	    public void deleteAll() {
	        mongoTemplate.remove(new Query(), COLLECTION);
	    }
	 
	    public User find(User user) {
	        Query query = new Query(Criteria.where("_id").is(user.getId()));
	        return mongoTemplate.findOne(query, User.class, COLLECTION);
	    }
	 
	    public List < User > findAll() {
	        return (List < User > ) mongoTemplate.findAll(User.class, COLLECTION);
	    }

		@Override
		public User findByUsername(String username) {
			 Query query = new Query(Criteria.where("username").is(username));
		        return mongoTemplate.findOne(query, User.class, COLLECTION);
		}

		@Override
		public User findByEmail(String email) {
			 Query query = new Query(Criteria.where("email").is(email));
		        return mongoTemplate.findOne(query, User.class, COLLECTION);
		}
	 

}
