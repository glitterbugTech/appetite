package com.websystique.springmvc.repo;

import java.util.List;



import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.websystique.springmvc.model.User;


public interface UserRepository  {

    public User findByUsername(String username);
    public User findByEmail(String email);
    public List < User > findAll();
    public void create(User user);
    public void update(User user);
    public void delete(User user);
	public void deleteAll();
    

}